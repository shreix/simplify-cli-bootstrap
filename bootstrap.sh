#!/usr/bin/env bash
set -e
apt-get update -y
apt-get install -y git-core curl zlib1g-dev libxml2-dev libxslt1-dev openssl nodejs postgresql libpq-dev nginx build-essential
cli_node=`curl https://bitbucket.org/api/1.0/repositories/shreix/simplify-cli-bootstrap/changesets/\?limit\=1 | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["changesets"][0]["node"]'`
wget https://bitbucket.org/shreix/simplify-cli-bootstrap/raw/$cli_node/pg_hba.conf
mv pg_hba.conf /etc/postgresql/9.1/main/pg_hba.conf
service postgresql restart
echo "enter bitbucket repo user"
read repo_user
echo "enter repo name"
read repo_name
echo "enter bitbucket username"
read username
echo "enter password"
read password
node=`curl --user $username:$password https://bitbucket.org/api/1.0/repositories/$repo_user/$repo_name/changesets/\?limit\=1 | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["changesets"][0]["node"]'`
simplify_url="https://bitbucket.org/$repo_user/$repo_name"
wget $simplify_url/raw/$node/config/deploy_setup.sh --user=$username --password=$password
sh deploy_setup.sh $username $password $simplify_url $node
rm deploy_setup.sh
rm bootstrap.sh